import argparse
import time
import cv2
import numpy as np

from pose_estimation import PoseEstimation
from pose_model_serializer import PoseModelSerializer
from pose_recognition_GMM import PoseRecognitionGMM
from pose_game import PoseGame
from pepper_socket_interface import PepperSocketInterface

from utils import *


def use_webcam(pose_estimation, pose_serializer, pose_recognition, pose_game):
    pose_game.start()
    fps_time = 0
    cam = cv2.VideoCapture(0)
    _, image = cam.read()
    while True:
        _, image = cam.read()
        humans = pose_estimation.get_humans(image)
        image = pose_estimation.draw_humans(image, humans)

        if len(humans) > 0:
            pose = pose_serializer.load_pose_from_OpenPose_model(humans[0])
            if pose:
                normalized_pose = pose.get_pose()
                predicted_pose = pose_recognition.predict_pose(normalized_pose)
                if pose_game.play(predicted_pose) is 'GAME_OVER':
                    break
        cv2.putText(image,
                    "FPS: %f" % (1.0 / (time.time() - fps_time)),
                    (10, 10),  cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                    (0, 255, 0), 2)
        cv2.imshow('result', image)
        fps_time = time.time()
        if cv2.waitKey(1) == 27:
            break
    cv2.destroyAllWindows()

#TODO change the name of open_pose_model arg
def use_video(pose_estimation, pose_serializer, pose_recognition, open_pose_model, pose_game, skeleton_only=False):
    pose_game.start()
    cap = cv2.VideoCapture(open_pose_model)
    if cap.isOpened() is False:
        print("Error opening video stream or file")
    fps_time = 0
    while cap.isOpened():
        _, image = cap.read()
        if image is not None:
            humans = pose_estimation.get_humans(image)
            if skeleton_only:
                image = np.zeros(image.shape)
            image = pose_estimation.draw_humans(image, humans)
            if len(humans) > 0:
                pose = pose_serializer.load_pose_from_OpenPose_model(humans[0])
                if pose:
                    normalized_pose = pose.get_pose()
                    predicted_pose = pose_recognition.predict_pose(normalized_pose)
                    if pose_game.play(predicted_pose) is 'GAME_OVER':
                        break

            cv2.putText(image, "FPS: %f" % (1.0 / (time.time() - fps_time)), (10, 10),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv2.imshow('Result', image)
        fps_time = time.time()
        if cv2.waitKey(1) == 27:
            break
    cv2.destroyAllWindows()


def use_pepper(pose_estimation, pose_serializer, pose_recognition, pose_game, pepper_interface):
    pose_game.start()
    fps_time = 0
    while True:
        image = pepper_interface.get_pepper_video()
        if image is None or image == b'PONG':
            pass
        else:
            humans = pose_estimation.get_humans(image)
            image = pose_estimation.draw_humans(image, humans)
            if len(humans) > 0:
                pose = pose_serializer.load_pose_from_OpenPose_model(humans[0])
                if pose:
                    normalized_pose = pose.get_pose()
                    predicted_pose = pose_recognition.predict_pose(normalized_pose)
                    print(predicted_pose)
                    print(predicted_pose)
                    if pose_game.play(predicted_pose) is 'GAME_OVER':
                        break
            cv2.putText(image, "FPS: %f" % (1.0 / (time.time() - fps_time)), (10, 10),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv2.imshow('Result', image)
            cv2.waitKey(10)
        fps_time = time.time()
        if cv2.waitKey(1) == 27:
            break
    cv2.destroyAllWindows()

DEFAULT_DATA_DIM = DATA_DIM_WITHOUT_HIP
NORMALIZATION_DEFAULT_METHOD = 'Nose_Neck__RShoulder_LShoulder'
GMM_DEFAULT_MODEL_PATH = './Nose_Neck__RShoulder_LShoulder_tied.joblib'
OPEN_POSE_DEFAULT_MODEL = 'mobilenet_thin'
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='pose-recognition')
    parser.add_argument('--source', type=str, default=0, help='0 for webcam otherwise path to the video')
    parser.add_argument('--open-pose-model', type=str, default=OPEN_POSE_DEFAULT_MODEL, help='cmu / mobilenet_thin / mobilenet_v2_large / mobilenet_v2_small')
    parser.add_argument('--normalization-method', type=str, default=NORMALIZATION_DEFAULT_METHOD, help='Normalization method, this will depend on the chosen model')
    parser.add_argument('--gmm-model', type=str, default=GMM_DEFAULT_MODEL_PATH, help='GMM model file path')
    parser.add_argument('--data-dim', type=int, default=DEFAULT_DATA_DIM, help='Either 14 or 16 depending on the GMM model')
    args = parser.parse_args()

    # OpenPose estimator setup
    pose_estimation = PoseEstimation(model=args.open_pose_model)
    # Model serializer setup
    pose_serializer = PoseModelSerializer(normalization_method=args.normalization_method, data_dim=args.data_dim)
    # GMM pose recognition setup
    pose_recognition = PoseRecognitionGMM(load_path=args.gmm_model)

    # Connect to Pepper
    if args.source == 'pepper1':
        pepper_interface = PepperSocketInterface(args.source)
        pose_game = PoseGame(pepper_interface, mode='MODE_2', player_name='Patricio')
        use_pepper(pose_estimation, pose_serializer, pose_recognition, pose_game, pepper_interface)
    elif args.source is 0:
        print(args.source)
        pose_game = PoseGame(None, mode='MODE_1', player_name='Patricio')
        use_webcam(pose_estimation, pose_serializer, pose_recognition, pose_game)
    else:
        print(args.source)
        pose_game = PoseGame(None, mode='MODE_1', player_name='Patricio')
        use_video(pose_estimation, pose_serializer, pose_recognition, args.source, pose_game, False)
