import numpy as np

from utils import logger, get_keypoint 
from utils import NORMALIZATION_METHODS, DATA_DIM_FULL, BODY_CENTER_KEYPOINT, USED_BODY_KEYPOINTS_FULL, DATA_DIM_WITHOUT_HIP, USED_BODY_KEYPOINTS_WITHOUT_HIP, POSES
from pose_model import PoseModel



class PoseModelSerializer:
    # TODO review default values
    def __init__(self,
                 dataset_path='',
                 dataset_max_size=20,
                 normalization_method=NORMALIZATION_METHODS[0],
                 data_dim=DATA_DIM_FULL):
        self.dataset_path_ = dataset_path
        self.dataset_max_size_ = dataset_max_size
        self.normalization_method_ = normalization_method
        self.data_dim_ = data_dim
        self.used_body_keypoints_ = []
        if self.data_dim_ is DATA_DIM_FULL:
            self.used_body_keypoints_ = USED_BODY_KEYPOINTS_FULL
        elif self.data_dim_ is DATA_DIM_WITHOUT_HIP:
            self.used_body_keypoints_ = USED_BODY_KEYPOINTS_WITHOUT_HIP

    def __repr__(self):
        return logger('PoseModelSerializer',
                      {'dataset_path': self.dataset_path_,
                       'dataset_max_size': self.dataset_max_size_,
                       'data_dim_': self.data_dim_})

    def get_dataset_path(self, dataset_path):
        return self.dataset_path_

    def get_dataset_max_size(self, dataset_max_size):
        return self.dataset_max_size_

    def get_normalization_method(self):
        return self.normalization_method_

    def get_data_dim(self):
        return self.data_dim_

    def set_dataset_path(self, dataset_path):
        self.dataset_path_ = dataset_path

    def set_dataset_max_size(self, dataset_max_size):
        self.dataset_max_size_ = dataset_max_size

    def set_normalization_method(self, normalization_method):
        self.normalization_method_ = normalization_method

    def set_data_dim(self, dim):
        self.data_dim_ = dim

    def parse_line(self, line):
        body_parts = line.replace("[", "")
        body_parts = body_parts.replace("]", "")
        body_parts = body_parts.replace(" ", "")
        body_parts = body_parts.split("BodyPart:")
        body_parts.pop(0)
        return body_parts

    def parse_position(self, pos):
        # The number 11 represent the max line
        pos = pos[:11]
        pos = pos.replace("(", "")
        pos = pos.replace(")", "")
        pos = pos.split(',')
        return pos

    def load_pose_from_file(self, filename):
        # Extract data from file
        dataset_file = open(filename, "r")        
        body_parts = self.parse_line(dataset_file.readline())
        human_pose = PoseModel(self.normalization_method_, data_dim=self.data_dim_)
        body_center_keypoint = get_keypoint(BODY_CENTER_KEYPOINT) # TODO Upper case this method
        dataset_file.close()
        # Iterate along the bodyparts data from the file
        index = 0
        for body_part in body_parts:
            data = body_part.split("-")
            body_part_keypoint = int(data[0])
            if body_part_keypoint in self.used_body_keypoints_:
                position = self.parse_position(data[1])
                if body_part_keypoint is body_center_keypoint:
                    human_pose.set_body_center(*position)
                else:
                    human_pose.set_pose(index, *position)
                    index += 1
        # Validate that the array has the right length
        if index is int(DATA_DIM_WITHOUT_HIP/2) and self.data_dim_ is DATA_DIM_WITHOUT_HIP:
            human_pose.normalize_values()
            return human_pose
        elif index is int(DATA_DIM_FULL/2) and self.data_dim_ is DATA_DIM_FULL:
            human_pose.normalize_values()
            return human_pose
        return None

    def load_pose_from_OpenPose_model(self, human):
        human_pose = PoseModel(self.normalization_method_, data_dim=self.data_dim_)
        body_center_keypoint = get_keypoint(BODY_CENTER_KEYPOINT)
        index = 0
        for body_parts_key in human.body_parts:
            if int(body_parts_key) in self.used_body_keypoints_:
                if int(body_parts_key) is body_center_keypoint:
                    keypoint_pos = human.body_parts[body_parts_key]
                    position = (keypoint_pos.x, keypoint_pos.y)
                    human_pose.set_body_center(*position)
                else:
                    keypoint_pos = human.body_parts[body_parts_key]
                    position = (keypoint_pos.x, keypoint_pos.y)
                    human_pose.set_pose(index, *position)
                    index += 1
        # Validate that the array has the right length
        if index is int(DATA_DIM_WITHOUT_HIP/2) and self.data_dim_ is DATA_DIM_WITHOUT_HIP:
            human_pose.normalize_values()
            return human_pose
        elif index is int(DATA_DIM_FULL/2) and self.data_dim_ is DATA_DIM_FULL:
            human_pose.normalize_values()
            return human_pose
        return None

    def build_pose_filname(self, file_num):
        #TODO Ideally someday this should be more compact
        filename = '' 
        if file_num < 9:
            filename = '00' + str(file_num + 1) + '.txt'
        elif file_num >= 9 and file_num - 1 < 99:
            filename = '0' + str(file_num + 1) + '.txt'
        elif file_num >= 99:
            filename = str(file_num + 1) + '.txt'
        return filename

    def load_pose_dataset(self, path):
        humans = []
        for file_num in range(self.dataset_max_size_):
            filename = self.build_pose_filname(file_num)
            human = self.load_pose_from_file(path + filename)
            # if the data is not valid it doesn't get into the list
            if human is not None:
                humans.append(human)
        return humans

    def load_all_poses_from_dataset(self):
        human_poses = {}
        for key_pose in POSES:
            pose = POSES[key_pose]
            path = self.dataset_path_ + "pose_" + str(key_pose) + "/"
            human_poses[pose] = self.load_pose_dataset(path)
        return human_poses
