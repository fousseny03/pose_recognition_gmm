import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

from sklearn import datasets
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import StratifiedKFold
from joblib import dump


colors = ['navy', 'turquoise', 'darkorange']

def make_ellipses(gmm, ax):
    for n, color in enumerate('bgrcmykw'):
        if color is 'w':
            color = '#bababa'
        if gmm.covariance_type == 'full':
            covariances = gmm.covariances_[n][:2, :2]
        elif gmm.covariance_type == 'tied':
            covariances = gmm.covariances_[:2, :2]
        elif gmm.covariance_type == 'diag':
            covariances = np.diag(gmm.covariances_[n][:2])
        elif gmm.covariance_type == 'spherical':
            covariances = np.eye(gmm.means_.shape[1]) * gmm.covariances_[n]
        v, w = np.linalg.eigh(covariances)
        u = w[0] / np.linalg.norm(w[0])
        angle = np.arctan2(u[1], u[0])
        angle = 180 * angle / np.pi  # convert to degrees
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        ell = mpl.patches.Ellipse(gmm.means_[n, :2], v[0], v[1], 180 + angle, color=color)
        ell.set_clip_box(ax.bbox)
        ell.set_alpha(0.5)
        ax.add_artist(ell)
        ax.set_aspect('equal', 'datalim')


DATA_DIM = 16

def normalize_values(human_pose, body_center):
    for element in range(DATA_DIM):
        height = abs(human_pose[1] - human_pose[15])
        width = abs(human_pose[2] - human_pose[8])#todo REVOIR
        if element%2 :
            human_pose[element] = (human_pose[element] - body_center[0]) / 1
        else:
            human_pose[element] = (human_pose[element] - body_center[1]) / 1
        print(len(human_pose))

def get_pose_from_file(filename):
    # Extract data from file
    file = open(filename, "r")
    body_parts = file.readline().replace("[","").replace("]","").replace(" ","")
    body_parts = body_parts.split("BodyPart:")
    body_parts.pop(0)
    # Create pose info
    human_pose = np.zeros(DATA_DIM)
    used_keypoints = [0, 1, 2, 3, 4, 5, 6, 7, 8]
    body_center = np.zeros(2)
    index = 0
    for body_part in body_parts:
        data = body_part.split("-")
        if int(data[0]) in used_keypoints:
            position = np.array(data[1][:11].replace("(","").replace(")","").split(','))
            if int(data[0]) == 1: #extract the center of the body
                body_center[0] = -float(position[0])
                body_center[1] = float(position[1])
            else:
                human_pose[2*index] = -float(position[0])
                human_pose[2*index+1] = float(position[1])
                index += 1
    
    normalize_values(human_pose, body_center)

    return human_pose

def get_dataset(pose_class, dataset_size, poses, target):
    for image_num in range(dataset_size):
        if image_num+1 > 9 :
            filename = "./dataset/pose_"+ str(pose_class) +"/0" + str(image_num+1) + ".txt"
        else:
            filename = "./dataset/pose_"+ str(pose_class) +"/00" + str(image_num+1) + ".txt"
        aux_array = get_pose_from_file(filename)
        print(aux_array)
        # Appending data through the reference argument
        poses.append(aux_array)
        target.append(pose_class)


def plot_joints_per_pose(poses):
    for joint in range(8):
        plt.figure()
        for i, color in enumerate('bgrcmykw'):
            h = plt.subplot(1, 1, 1)
            make_ellipses(classifier, h)
            if color is 'w':
                color = '#bababa' 
            plt.plot(poses[20*i:20*(i+1), joint*2], poses[20*i:20*(i+1), joint*2 + 1], 'x', color=color)
        plt.show()


poses_classes = [0, 1, 2, 3, 4, 5, 6, 7]
poses = []
target = []

# for label in poses_classes:
#     get_dataset(poses_classes[label], 20, poses, target)

get_dataset(0, 20, poses, target)

poses = np.array(poses)
target = np.array(target)

skf = StratifiedKFold(n_splits=4)
skf.get_n_splits(target)
train_index, test_index = next(iter(skf.split(poses, target)))

X_train = poses[train_index]
y_train = target[train_index]
X_test = poses[test_index]
y_test = target[test_index]

n_classes = len(np.unique(y_train))



# 'spherical', 'diag', 'tied', 'full'
estimator = GaussianMixture(n_components=n_classes, covariance_type="full", 
                                init_params='random', max_iter=20, random_state=0)

estimator.means_init = np.array([X_train[y_train == i].mean(axis=0)
                                    for i in range(n_classes)])

estimator.fit(X_train)
dump(estimator, 'model.joblib') 

h = plt.subplot(1,1,1)
# make_ellipses(estimator, h)

for n, color in enumerate('bgrcmykw'):
    if color is 'w':
        color = '#bababa' 
    data = poses[target == n]
    plt.scatter(data[:, 2], data[:, 3], 10, color=color,
                    label=poses_classes[n])

# for n, color in enumerate('bgrcmykw'):
#     if color is 'w':
#         color = '#bababa'
#     data = X_test[y_test == n]
#     plt.plot(data[:, 0], data[:, 1], 'x', color=color)
    
y_train_pred = estimator.predict(X_train)
train_accuracy = np.mean(y_train_pred.ravel() == y_train.ravel()) * 100
print(train_accuracy)

y_test_pred = estimator.predict(X_test)
test_accuracy = np.mean(y_test_pred.ravel() == y_test.ravel()) * 100
print(test_accuracy)


plt.show()