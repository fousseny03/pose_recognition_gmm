import sys
import os

from tf_pose import common
import cv2
import numpy as np
from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path


DATASET_PATH = './dataset/images_s5/'
RAW_IMAGES_PATH = 'raw_images'
PROCESSED_IMAGES_PATH = 'processed_images/'
PROCESSED_IMAGES_DATA_PATH = 'processed_images_data/'
POSE_NUM = 8

def process_image(pose_path, image_path, estimator):
    image_file_path = pose_path + RAW_IMAGES_PATH + '/' + image_path
    print("Processing file: " + image_file_path)
    image = common.read_imgfile(image_file_path, None, None)
    if image is None:
        print("***Processing Error: Image can not be read, path: " + image_file_path)
        sys.exit(-1)

    humans = estimator.inference(image, resize_to_default=False, upsample_size=4.0)
    export_data(humans, pose_path + PROCESSED_IMAGES_DATA_PATH + image_path)

    image = TfPoseEstimator.draw_humans(image, humans, imgcopy=False)
    print(pose_path + PROCESSED_IMAGES_PATH + image_path)
    cv2.imwrite(pose_path + PROCESSED_IMAGES_PATH + image_path , image)
    pass

def export_data(humans, file_path):
    file_path = "." + file_path.split('.')[1] + ".txt"
    print("Model saved to the data file: " + file_path)
    f= open(file_path,"w+")
    f.write(str(humans))
    f.close()
    pass

def process_dataset(estimator):
    pose_path = 'pose_'
    for pose_num in range(POSE_NUM):
        pose_dir_name = DATASET_PATH + pose_path + str(pose_num) + '/'
        for images in os.walk(pose_dir_name):
            if (pose_dir_name + RAW_IMAGES_PATH) in images:
                for image_name in images[2]:
                    process_image(pose_dir_name, image_name, estimator)
    pass

if __name__ == '__main__':
    estimator = TfPoseEstimator(get_graph_path("cmu"), target_size=(432, 368))
    process_dataset(estimator)
