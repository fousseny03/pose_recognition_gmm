import matplotlib.pyplot as plt

from utils import NORMALISED_KEYPOINTS_DICT, POSES, logger

COLORS = [
    'blue',
    'green',
    'red',
    'cyan',
    'magenta',
    'yellow',
    'black',
    'brown']

class DatasetPlotters:
    def __init__(self, output_path, show_graph=False, save_graph=False, data_dim=7):
        self.output_path_ = output_path
        self.show_graph_ = show_graph
        self.save_graph_ = save_graph
        self.data_dim_ = data_dim

    def __repr__(self):
        return logger('DatasetPlotters',
                      {'output_path': self.output_path_,
                       'show_graph': self.show_graph_,
                       'save_graph': self.save_graph_})

    def get_output_path(self):
        return self.output_path_

    def get_show_graph_status(self):
        return self.show_graph_

    def get_save_graph_status(self):
        return self.save_graph_

    def set_output_path(self, output_path):
        self.output_path_ = output_path

    def set_save_graph_status(self, save_graph):
        self.save_graph_ = save_graph

    def set_show_graph_status(self, show_graph):
        self.show_graph_ = show_graph

    def plot_keypoints_of_one_pose(self, pose, humans, label_pos=None):
        plt.figure()
        for human in humans[POSES[pose]]:
            human_pose = human.get_pose()
            for keypoint in range(self.data_dim_/2):
                human_pos_x = human_pose[2 * keypoint]
                human_pos_y = human_pose[2 * keypoint + 1]
                plt.plot(
                    human_pos_x,
                    human_pos_y,
                    marker='o',
                    markersize=10,
                    color=COLORS[keypoint])
                if label_pos:
                    plt.text(label_pos[0],
                             keypoint * label_pos[1] + label_pos[2],
                             NORMALISED_KEYPOINTS_DICT[keypoint],
                             color=COLORS[keypoint])
        plt.title('keypoints of pose ' + POSES[pose])
        if self.show_graph_ is True:
            plt.show()
        if self.save_graph_ is True:
            plt.savefig(self.output_path_ + 'pose_' + str(pose) + '.png')

    def plot_keypoints_of_all_pose(self, humans, labels_pos=None):
        for pose in range(len(POSES)):
            if labels_pos:
                self.plot_keypoints_of_one_pose(pose, humans, labels_pos[pose])
            else:
                self.plot_keypoints_of_one_pose(pose, humans, None)

    def plot_keypoints_every_pose(self, keypoint, human_poses, label_pos=None):
        plt.figure()
        for pose_key in POSES:
            for human in human_poses[POSES[pose_key]]:
                pose_coord = human.get_pose()
                human_pos_x = pose_coord[2 * keypoint]
                human_pos_y = pose_coord[2 * keypoint + 1]
                plt.plot(
                    human_pos_x,
                    human_pos_y,
                    marker='o',
                    markersize=10,
                    color=COLORS[pose_key])
                if label_pos:
                    plt.text(label_pos[0],
                             pose_key * label_pos[1] + label_pos[2],
                             POSES[pose_key],
                             color=COLORS[pose_key])
        plt.title(
            'Position of ' +
            NORMALISED_KEYPOINTS_DICT[keypoint] +
            '(' +
            str(keypoint) +
            ')')
        if self.show_graph_ is True:
            plt.show()
        if self.save_graph_ is True:
            plt.savefig(
                self.output_path_ +
                'keypoint_' +
                str(keypoint) +
                '.png')

    def plot_all_keypoints_every_pose(self, human_poses, labels_pos):
        for keypoint in range(self.data_dim_/2):
            if labels_pos:
                self.plot_keypoints_every_pose(
                    keypoint, human_poses, labels_pos[keypoint])
            else:
                self.plot_keypoints_every_pose(
                    keypoint, human_poses, None)

    def plot_all(self, human_poses_by_normalization):
        path_graphs = './graphs/'
        for method in human_poses_by_normalization:
            human_poses = human_poses_by_normalization[method]
            self.set_output_path(path_graphs + method + '/keypoints_per_pose/')
            self.plot_all_keypoints_every_pose(human_poses, None)
            # Plot keypoints
            self.set_output_path(path_graphs + method + '/keypoints_of_every_pose/')
            self.plot_keypoints_of_all_pose(human_poses, None)
