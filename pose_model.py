import numpy as np

from utils import BODY_POSE_DIM, NORMALIZATION_METHODS, logger, BODY_CENTER_DIM, DATA_DIM_FULL, DATA_DIM_WITHOUT_HIP

NORMALIZATION_KEYPOINTS = {
    'Nose_Y': 1,
    'MidHip_Y': 15,
    'RShoulder_X': 2,
    'LShoulder_X': 8}


class PoseModel:
    def __init__(
            self,
            nomalization_method=NORMALIZATION_METHODS[0],
            data_dim=DATA_DIM_FULL):
        self.pose_ = np.zeros(data_dim)
        self.body_center_ = np.zeros(BODY_CENTER_DIM)
        self.nomalization_method_ = nomalization_method
        self.data_dim_ = data_dim

    def __repr__(self):
        return logger('PoseModel',
                      {'nomalization_method_': self.nomalization_method_,
                       'body_center_': self.body_center_,
                       'pose_': self.pose_,
                       'data_dim_': self.data_dim_})

    def get_body_center(self):
        return self.body_center_

    def get_pose(self):
        return self.pose_

    def get_nomalization_method(self):
        return self.nomalization_method_

    def get_data_dim(self):
        return self.data_dim_

    def set_pose(self, i, *p):
        (x, y) = p
        self.pose_[2 * i] = float(x)
        self.pose_[2 * i + 1] = float(y)

    def set_pose_array(self, pose):
        self.pose_ = pose

    def set_data_dim(self, dim):
        self.data_dim_ = dim

    def set_body_center(self, *bc):
        self.body_center_[0] = float(bc[0])
        self.body_center_[1] = float(bc[1])

    def set_nomalization_method(self, nomalization_method):
        self.nomalization_method_ = nomalization_method

    # Normalization methods
    def RShoulder_LShoulder__RShoulder_LShoulder_norm(self):
        width = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['RShoulder_X']] -
            self.pose_[
                NORMALIZATION_KEYPOINTS['LShoulder_X']])
        height = width
        return height, width

    def RShoulder_LShoulder__3xRShoulder_LShoulder_norm(self):
        width = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['RShoulder_X']] -
            self.pose_[
                NORMALIZATION_KEYPOINTS['LShoulder_X']])
        height = 3 * width
        return height, width

    def Nose_MidHip__RShoulder_LShoulder_norm(self):
        height = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['Nose_Y']] -
            self.pose_[
                NORMALIZATION_KEYPOINTS['MidHip_Y']])
        width = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['RShoulder_X']] -
            self.pose_[
                NORMALIZATION_KEYPOINTS['LShoulder_X']])
        return height, width

    def Nose_MidHip__Nose_MidHip_norm(self):
        height = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['Nose_Y']] -
            self.pose_[
                NORMALIZATION_KEYPOINTS['MidHip_Y']])
        width = height
        return height, width

    def Nose_MidHip__3xNose_MidHip_norm(self):
        height = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['Nose_Y']] -
            self.pose_[
                NORMALIZATION_KEYPOINTS['MidHip_Y']])
        width = 3 * height
        return height, width

    def Nose_Neck__RShoulder_LShoulder_norm(self):
        height = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['Nose_Y']] -
            self.body_center_[1])
        width = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['RShoulder_X']] -
            self.pose_[
                NORMALIZATION_KEYPOINTS['LShoulder_X']])
        return height, width

    def Nose_Neck__Nose_Neck_norm(self):
        height = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['Nose_Y']] -
            self.body_center_[1])
        width = height
        return height, width

    def Nose_Neck__3xNose_Neck_norm(self):
        height = abs(
            self.pose_[
                NORMALIZATION_KEYPOINTS['Nose_Y']] -
            self.body_center_[1])
        width = 3 * height
        return height, width

    def normalize_values(self):
        for element in range(self.data_dim_):
            # Select the normalization method
            height = 1
            width = 1
            # RShoulder_LShoulder__RShoulder_LShoulder
            if self.nomalization_method_ is NORMALIZATION_METHODS[4]:
                height, width = self.RShoulder_LShoulder__RShoulder_LShoulder_norm()
            # RShoulder_LShoulder__3xRShoulder_LShoulder
            elif self.nomalization_method_ is NORMALIZATION_METHODS[5]:
                height, width = self.RShoulder_LShoulder__3xRShoulder_LShoulder_norm()
            # FULL mode (with the hip)
            elif self.data_dim_ is DATA_DIM_FULL:
                # Nose_MidHip__RShoulder_LShoulder
                if self.nomalization_method_ is NORMALIZATION_METHODS[1]:
                    height, width = self.Nose_MidHip__RShoulder_LShoulder_norm()
                # Nose_MidHip__Nose_MidHip
                elif self.nomalization_method_ is NORMALIZATION_METHODS[2]:
                    height, width = self.Nose_MidHip__Nose_MidHip_norm()
                # Nose_MidHip__3xNose_MidHip
                elif self.nomalization_method_ is NORMALIZATION_METHODS[3]:
                    height, width = self.Nose_MidHip__3xNose_MidHip_norm()
            # WITHOUT_HIP mode (without the hip)
            elif self.data_dim_ is DATA_DIM_WITHOUT_HIP:
                # Nose_Neck__RShoulder_LShoulder
                if self.nomalization_method_ is NORMALIZATION_METHODS[6]:
                    height, width = self.Nose_Neck__RShoulder_LShoulder_norm()
                # Nose_Neck__Nose_Neck
                elif self.nomalization_method_ is NORMALIZATION_METHODS[7]:
                    height, width = self.Nose_Neck__Nose_Neck_norm()
                # Nose_Neck__3xNose_Neck
                elif self.nomalization_method_ is NORMALIZATION_METHODS[8]:
                    height, width = self.Nose_Neck__3xNose_Neck_norm()

            if width == 0 or height == 0 or np.isinf(width) or np.isinf(height) or np.isnan(width) or np.isnan(height):
                return None
            # Compute the final result
            if element % 2:  # x
                self.pose_[element] = (
                    self.pose_[element] - self.body_center_[0]) / width
            else:  # y
                self.pose_[element] = (
                    self.pose_[element] - self.body_center_[1]) / height
        return self.pose_
